import math, time, pdb
import numpy as np
import pprint as pp
from os import system 
from random import shuffle, randrange, randint

DIR_UP     = 0
DIR_RIGHT  = 1
DIR_DOWN   = 2
DIR_LEFT   = 3

def p(*s):
  pp.pprint(s)

def byebug():
  pdb.set_trace()

def build_maze(data):
  ver, hor, w, h = data
  s = ""
  for (a, b) in zip(list(hor), list(ver)):
    s += ''.join(a + ['\n'] + b + ['\n'])
  return (s, (list(ver), list(hor), w, h))

def can_move(_pos, data, move):
  ver, hor, w, h = data
  if not (0 <= _pos[0] < h and 0 <= _pos[1] < w): return False 
  return {
    DIR_UP:     hor[_pos[0]][_pos[1]][1:3] != "--",
    DIR_RIGHT:  ver[_pos[0]][_pos[1] + 1][0] != "|",
    DIR_DOWN:   hor[_pos[0] + 1][_pos[1]][1:3] != "--",
    DIR_LEFT:   ver[_pos[0]][_pos[1]][0] != "|"
  }[move]

def move(_pos, data, move):
  ver, hor, w, h = data
  if not (0 <= _pos[0] < h and 0 <= _pos[1] < w) or not(can_move(_pos, data, move)): return _pos
  return {
    DIR_UP:     (_pos[0] - 1, _pos[1]) if _pos[0] > 0 else _pos,
    DIR_RIGHT:  (_pos[0], _pos[1] + 1) if _pos[1] < (w - 1) else _pos,
    DIR_DOWN:   (_pos[0] + 1, _pos[1]) if _pos[0] < (h - 1) else _pos,
    DIR_LEFT:   (_pos[0], _pos[1] - 1) if _pos[1] > 0 else _pos
  }[move]

def move_pointer(_from, _to, data):
  ver, hor, w, h = data
  ver = list(ver)
  hor = list(hor)
  ver[_from[0]][_from[1]] = "%s  " %ver[_from[0]][_from[1]][0]
  ver[_to[0]][_to[1]] = "%s##" %ver[_to[0]][_to[1]][0]
  return (ver, hor, w, h)

# The original make_maze took from `https://rosettacode.org/wiki/Maze_generation#Python`
def make_maze(w = 16, h = 8, pos = [0, 0], enter = 1, exit = 2):
  vis = [[0] * w + [1] for _ in range(h)] + [[1] * (w + 1)]
  ver = [["|  "] * w + ['|'] for _ in range(h)] + [[]]
  hor = [["+--"] * w + ['+'] for _ in range(h + 1)]

  def walk(x, y):
      vis[y][x] = 1
      d = [(x - 1, y), (x, y + 1), (x + 1, y), (x, y - 1)]
      shuffle(d)
      for (xx, yy) in d:
          if vis[yy][xx]: continue
          if xx == x: hor[max(y, yy)][x] = "+  "
          if yy == y: ver[y][max(x, xx)] = "   "
          walk(xx, yy)

  walk(randrange(w), randrange(h))

  ver[pos[0]][pos[1]] = "%s##" %ver[pos[0]][pos[1]][0]
  ver[enter][0] = ">%s" %ver[enter][0][1:3]
  ver[exit][w] = ">"

  return build_maze((ver, hor, w, h))
 
def reload_display(data):
  system("clear")
  maze, data = build_maze(data)
  print(maze) 

if __name__ == '__main__':
  w = 16
  h = 8
  start = (randint(0, h), 0)
  finish = (randint(0, h), w - 1)
  pos = start
  maze, data = make_maze(16, 8, pos, start[0], finish[0])
  print(maze)
  _input = ""
  while(_input != "q"):
    reload_display(data)
    _input = raw_input("Enter a direction [w, d, s, a, q = quit]: ").lower()
    _options = {
      "w": DIR_UP,
      "d": DIR_RIGHT,
      "s": DIR_DOWN,
      "a": DIR_LEFT
    }
    if _input in _options:
      _dir = _options[_input]
      _new_pos = move(pos, data, _dir)
      data = move_pointer(pos, _new_pos, data)
      pos = _new_pos
      if pos == finish:
        reload_display(data)
        print("[ YOU WON! ]")
        exit(0)